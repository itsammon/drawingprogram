/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.helpers;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 * Filter to handle only getting image files
 * @author ammon
 * Borrowed from FileChooserTutorial
 */
public class ImageFilter extends FileFilter {
    
    //Accept all directories and all gif, jpg, tiff, or png files.
    @Override
    public boolean accept(File f) {
        // Allow directories to work
        if (f.isDirectory()) {
            return true;
        }
        
        // List of allowable extensions
        String extension = Utils.getExtension(f);
        if (extension != null) {
            if (extension.equals(Utils.tiff) ||
                extension.equals(Utils.tif) ||
                extension.equals(Utils.gif) ||
                extension.equals(Utils.jpeg) ||
                extension.equals(Utils.jpg) ||
                extension.equals(Utils.png)) {
                    return true;
            } else {
                return false;
            }
        }
        
        return false;
    }
    //The description of this filter
    @Override
    public String getDescription() {
        return "Image files";
    }
}
