/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.Dimension;
import javax.swing.JFrame;
import Drawing.Drawing;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Timer;

/**
 *
 * @author ammon
 */
public class MainFrame extends JFrame implements DrawingFrame{
    private GraphicsPanel dPanel;
    private Drawing drawing;
    private Timer refreshTimer;
    
    /**
     * Create the main frame of the application
     * @param name The title of the window
     */
    public MainFrame(String name)
    {
        super(name);
        try {
            drawing = new Drawing(null, Color.WHITE);
        } catch (IOException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Create the drawing listener for events
        DrawingActionListener listener = new DrawingActionListener(this);
         
        // Add a shutdown hook to close network comunications when the program exits
        //addShutdownBehavoir();
        
        // Set minimum size and behaviors
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(100, 100));
        setSize(new Dimension(500,500));

        // Set up the content pane
        setLayout(new BorderLayout());
        getContentPane().setSize(500, 500);
        getContentPane().setLayout(new BorderLayout());
        
        // Add in the Drawing panel
        dPanel = new GraphicsPanel(drawing);
        dPanel.addMouseListener(listener);
        dPanel.addMouseMotionListener(listener);
        dPanel.requestFocus();
        getContentPane().add(dPanel);
        
        // Add the menu bars
        MenuBar bar = new MenuBar(listener);
        setJMenuBar(bar);
        
        // Create a refresh timer
        createRefreshTimer();
        
        // Set the loacation of the frame
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        
        // Start the refreshTimer
        refreshTimer.start();
    }
    
    /**
     * Creates a new refresh timer that refreshes the window
     */
    private void createRefreshTimer()
    {
        refreshTimer = new Timer(100,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // When the timer triggers, refresh the drawing
                displayDrawing();
            }
        });
    }
    
    /**
     * Cause the drawing to be updated
     */
    private void displayDrawing()
    {
        if (drawing.isDirty())
        {
            this.setTitle(drawing.getName());
            revalidate();
        }
        repaint();
    }
    
    /**
     * Sets the drawing to be the specified drawing
     * @param drawing The new drawing
     */
    @Override
    public void setDrawing(Drawing drawing)
    {
        this.drawing = drawing;
        dPanel.setDrawing(drawing);
        this.setTitle(drawing.getName());
    }
    
    /**
     * Gets the drawing
     * @return The drawing object
     */
    @Override
    public Drawing getDrawing()
    {
        return drawing;
    }
}
