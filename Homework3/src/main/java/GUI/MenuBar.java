/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

/**
 *
 * @author ammon
 */
public class MenuBar extends JMenuBar{
    private JMenuItem menuItem;
    private JMenu menu;
    private final DrawingActionListener listener;
    
    /**
     * Create the menu bar
     * @param listen The listener to handle menu choices
     */
    public MenuBar(DrawingActionListener listen)
    {
        super();
        
        listener = listen;
        
        //Create and add each menu
        addFileMenu();
        addEditMenu();
        addDrawingMenu();
        addImageMenu();
    }
    
    /**
     * Create and add the file menu
     */
    private void addFileMenu()
    {
        menu = new JMenu("File");
        add(menu);
        
        menuItem = new JMenuItem("New Drawing");
        menuItem.setAccelerator(KeyStroke.getKeyStroke('N', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Load Drawing");
        menuItem.setAccelerator(KeyStroke.getKeyStroke('L', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Save Drawing");
        menuItem.setAccelerator(KeyStroke.getKeyStroke('S', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Set Output Location");
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Quit");
        menuItem.setAccelerator(KeyStroke.getKeyStroke('Q', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        menuItem.addActionListener(listener);
        menu.add(menuItem);
    }
    
    /**
     * Create and add the edit menu
     */
    private void addEditMenu()
    {
        menu = new JMenu("Edit");
        add(menu);
        
        menuItem = new JMenuItem("Undo");
        menuItem.setAccelerator(KeyStroke.getKeyStroke('U', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        menuItem.addActionListener(listener);
        menu.add(menuItem);
    }
    
    /**
     * Create and add the drawing menu
     */
    private void addDrawingMenu()
    {
        menu = new JMenu("Drawing");
        add(menu);
        
        menuItem = new JMenuItem("Set Background Image");
        menuItem.setAccelerator(KeyStroke.getKeyStroke('I', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Set Background Color");
        menuItem.setAccelerator(KeyStroke.getKeyStroke('B', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Clear Drawing");
        menuItem.addActionListener(listener);
        menu.add(menuItem);
    }
    
    /**
     * Create and add the image menu
     */
    private void addImageMenu()
    {
        menu = new JMenu("Image");
        add(menu);
        
        menuItem = new JMenuItem("Add Image");
        menuItem.setAccelerator(KeyStroke.getKeyStroke('A', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Delete Selected Images");
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Duplicate Image");
        menuItem.setAccelerator(KeyStroke.getKeyStroke('D', Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
        menuItem.addActionListener(listener);
        menu.add(menuItem);
        
        menuItem = new JMenuItem("Resize Image");
        menuItem.setAccelerator(KeyStroke.getKeyStroke('R', Toolkit.getDefaultToolkit ().getMenuShortcutKeyMask()));
        menuItem.addActionListener(listener);
        menu.add(menuItem);
    }
}
