/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Drawing.Drawing;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author ammon
 */
public class GraphicsPanel extends JPanel {
    private Drawing drawing; 
    
    /**
     * Setup the graphics panel
     * @param drawing The current drawing
     */
    public GraphicsPanel(Drawing drawing) {
        this.drawing = drawing;
        setPreferredSize(new Dimension(300, 300));
    }

    /**
     * Paints the panel
     * @param g The graphics objected passed in by repaint
     */
    @Override
    public void paintComponent(Graphics g) {
        setBackground(drawing.getBackgroundColor());
        super.paintComponent(g);
        
        drawing.draw(g);
        g.dispose();
    }
    
    /**
     * Set the drawing to be the new drawing
     * @param drawing 
     */
    public void setDrawing(Drawing drawing)
    {
        this.drawing = drawing;
    }
}
