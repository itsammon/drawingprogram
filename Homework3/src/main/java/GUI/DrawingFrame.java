/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Drawing.Drawing;

/**
 *
 * @author ammon
 */
public interface DrawingFrame {
    /**
     * Sets the drawing to be the specified drawing
     * @param drawing The new drawing
     */
    public void setDrawing(Drawing drawing);
    
    /**
     * Gets the drawing
     * @return The drawing object
     */
    public Drawing getDrawing();
}
