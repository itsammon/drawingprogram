/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Command.Command;
import Command.CommandFactory;
import Command.CommandInvoker;
import Drawing.ApplicationException;
import Drawing.Drawing;
import Drawing.Image;
import GUI.helpers.ImageFilter;
import IO.Amazon;
import IO.IOStrategy;
import IO.Json;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.event.MouseInputListener;

/**
 *
 * @author ammon
 */
public class DrawingActionListener implements MouseInputListener, ActionListener{
    private final CommandFactory cFactory;
    private CommandInvoker invoker;
    private Image image;
    private Point startPoint;
    private boolean dragged = false;
    private final DrawingFrame frame;
    private IOStrategy ioStrat = new Json();
    
    /**
     * Create the listener
     * @param frame The frame to listen to
     */
    public DrawingActionListener(DrawingFrame frame)
    {
        super();
        this.frame = frame;
        cFactory = new CommandFactory(frame.getDrawing());
        invoker = new CommandInvoker();
        invoker.start();
    }
    
    /**
     * Handle the menu choices
     * @param e The action event requested
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        JMenuItem source = (JMenuItem)(e.getSource());
        switch (source.getText()) {
            case "Quit":
                invoker.stop();
                System.exit(0);
            case "New Drawing":
                newDrawing();
                break;
            case "Load Drawing":
                loadDrawing();
                break;
            case "Save Drawing":
                saveDrawing();
                break;
            case "Set Output Location":
                setOutput();
                break;
            case "Undo":
                invoker.undo();
                break;
            case "Set Background Image":
                setBackgroundImage();
                break;
            case "Set Background Color":
                setBackgroundColor();
                break;
            case "Clear Drawing":
                clear();
                break;
            case "Add Image":
                addImage();
                break;
            case "Delete Selected Images":
                deleteSelected();
                break;
            case "Duplicate Image":
                duplicateImage();
                break;
            case "Resize Image":
                resizeImage();
                break;
            default:
                break;
        }
    }

    /**
     * Does nothing right now
     * @param e 
     */
    @Override
    public void mouseClicked(MouseEvent e) {
    }
    
    /**
     * Allow user to select images by clicking them
     * @param e The event that fired
     */
    @Override
    public void mousePressed(MouseEvent e) {
        Command cmd = null;
        image = frame.getDrawing().findImageAtPosition(e.getPoint());
        
        // If they clicked on an image select it. Otherwise unselect all images.
        if (image != null)
        {
            // Check to make sure it isn't already selected then select it
            if (!image.isSelected())
            {
                // Check to see if the shift key was pressed down
                if (!e.isShiftDown())
                {
                    cmd = cFactory.getCommand("SelectOne", image);
                }
                else
                {
                    cmd = cFactory.getCommand("Select", image);
                }
            }
        }
        else
        {
            cmd = cFactory.getCommand("DeselectAll");
        }
        invoker.enqueueCommand(cmd);
    }

    /**
     * Handle the mouse up for move commands
     * @param e The event that fired
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        // If the user finished a drag motion, then we need to handle it
        if (dragged)
        {
            Command cmd;
            // If we have an image selected when we ended, create a move command
            if (image != null)
            {
                cmd = cFactory.getCommand("Move", image, startPoint, e.getPoint());
                invoker.enqueueCommand(cmd);
            }
            dragged = false;
        }
    }

    /**
     * Does nothing right now
     * @param e 
     */
    @Override
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Does nothing right now
     * @param e 
     */
    @Override
    public void mouseExited(MouseEvent e) {
    }
    
    /**
     * 
     * @param e 
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        if (image != null)
        {
            if (dragged == false)
            {
                startPoint = new Point();
                startPoint.setLocation(image.getLocation());
                dragged = true;
            }
            try {
                frame.getDrawing().dirty();
                image.setLocation(e.getPoint());
            } catch (ApplicationException ex) {
                Logger.getLogger(DrawingActionListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Does nothing right now
     * @param e 
     */
    @Override
    public void mouseMoved(MouseEvent e) {
    }
    
    /**
     * Make a new drawing
     */
    private void newDrawing()
    {
        Command cmd = null;
        // Stop the command invoker
        invoker.stop();
        
        try {
            Color color = JColorChooser.showDialog(
                    null,
                    "Choose a Background Color",
                    Color.WHITE);
            java.awt.Image bgImage;
            if (color != null) {
                // Clear the new drawing and clean up the program
                frame.setDrawing(new Drawing(null, color));
            }

            JFileChooser fc = new JFileChooser();
            fc.setFileFilter(new ImageFilter());
            int returnVal = fc.showDialog(null, "Choose Background Image");
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                cmd = cFactory.getCommand("BackgroundImage", fc.getSelectedFile());
            }
            frame.getDrawing().dirty();
        } catch (IOException ex) {
            Logger.getLogger(DrawingActionListener.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Create and reset the command invoker
        invoker = new CommandInvoker();
        invoker.start();
        invoker.enqueueCommand(cmd);
    }
    
    /**
     * Load a drawing
     */
    private void loadDrawing()
    {
        String filename = "drawing";
        try {
            invoker.stop();
            
            // Get filename from user
            if (ioStrat.getClass() == Amazon.class)
            {
                filename = (String)JOptionPane.showInputDialog(null, "Name of File?",
                    "File Name", JOptionPane.QUESTION_MESSAGE, null, null, null); 
            }
            else if (ioStrat.getClass() == Json.class)
            {
                // Code for if we are using json on locale disk
                JFileChooser fc = new JFileChooser();
                int returnVal = fc.showOpenDialog(null);

                if (returnVal == JFileChooser.APPROVE_OPTION)
                    filename = fc.getSelectedFile().getAbsolutePath();
            }
            
            // Load the file
            if (filename != null)
            {
                Drawing draw = ioStrat.load(filename);
                if (draw != null)
                {
                    frame.setDrawing(draw);
                    cFactory.setDrawing(frame.getDrawing());
                }
            }
            
            invoker = new CommandInvoker();
        }
        catch (FileNotFoundException ex)
        {
            JOptionPane.showMessageDialog(null, "Unable to load file " + filename
                    + "! File does not exist!",
                    "Load File Error", JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Unable to load file " + filename
                    + "! Unable to parse file!", "Load File Error", JOptionPane.ERROR_MESSAGE);
        } 
        finally
        {
            invoker.start();
        }
    }
    
    /**
     * Save the Drawing
     */
    private void saveDrawing()
    {
        String filename = null;
        try {
            // Get filename from user
            if (ioStrat.getClass() == Amazon.class)
            {
                filename = (String)JOptionPane.showInputDialog(null, "Name of File?",
                    "File Name", JOptionPane.QUESTION_MESSAGE, null, null, null);
                frame.getDrawing().setName(filename);
            }
            else if (ioStrat.getClass() == Json.class)
            {
                JFileChooser fc = new JFileChooser();
                int returnVal = fc.showSaveDialog(null);

                if (returnVal == JFileChooser.APPROVE_OPTION)
                {
                    filename = fc.getSelectedFile().getAbsolutePath();
                    frame.getDrawing().setName(fc.getSelectedFile().getName());
                }
            }
            
            // Save the file
            if (filename != null)
            {
                ioStrat.save(frame.getDrawing(), filename);
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Unable to save drawing", "Load File Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * Set the background image of the drawing
     */
    private void setBackgroundImage()
    {
        Command cmd;
        java.awt.Image bgImage;
            
        JFileChooser fc = new JFileChooser();
        fc.setFileFilter(new ImageFilter());
        int returnVal = fc.showDialog(null, "Choose Background Image");
        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            cmd = cFactory.getCommand("BackgroundImage", fc.getSelectedFile());
            invoker.enqueueCommand(cmd);
        }
    }
    
    /**
     * Set the background color of the drawing
     */
    private void setBackgroundColor()
    {
        Command cmd;
        Color color = JColorChooser.showDialog(
                null,
                "Choose a Background Color",
                frame.getDrawing().getBackgroundColor());
        if (color != null)
        {
            cmd = cFactory.getCommand("BackgroundColor", color);
            invoker.enqueueCommand(cmd);
        }
    }

    /**
     * Clear the drawing
     */
    private void clear()
    {
        Command cmd;
        cmd = cFactory.getCommand("Clear");
        invoker.enqueueCommand(cmd);
    }
    
    /**
     * Add an image to the drawing
     */
    private void addImage()
    {
        try {
            // Get the possible types
            Object[] types = frame.getDrawing().factory.getPossibleTypes();
            Command cmd;
            // Create a menu to choose an image type
            String type = (String)JOptionPane.showInputDialog(
                    null,
                    "Choose a type of image to add",
                    "Select Image",
                    JOptionPane.PLAIN_MESSAGE,
                    null,
                    types,
                    "orange");
            // If they acutally chose a type
            if (type != null && type.length() > 0)
            {
                image = frame.getDrawing().factory.getImage(frame.getDrawing().factory.getExtrinsic(type));
            
                cmd = cFactory.getCommand("Add", image);
                invoker.enqueueCommand(cmd);
            }
        } catch (IOException ex) {
            Logger.getLogger(DrawingActionListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Duplicate the last image selected
     */
    private void duplicateImage()
    {
        if (image != null)
        {
            Command cmd = cFactory.getCommand("Duplicate", image);
            invoker.enqueueCommand(cmd);
        }
    }
    
    /**
     * Delete all selected images
     */
    private void deleteSelected()
    {
        Command cmd;
        cmd = cFactory.getCommand("RemoveSelected");
        invoker.enqueueCommand(cmd);
    }
    
    /**
     * Resize the last selected image
     */
    private void resizeImage()
    {
        if (image != null)
        {
            // Get the new size
            String[] input = twoInputMessage("Resize Image", "Width", "Height");
            if (input != null)
            {
                try
                {
                    int width = Integer.parseInt(input[0]);
                    int height = Integer.parseInt(input[1]);
                    if (width > 0 && height > 0)
                    {
                        Command cmd = cFactory.getCommand("Resize", image, width, height);
                        invoker.enqueueCommand(cmd);
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null, "Size values must be greater than 0!", "Bad Input", JOptionPane.INFORMATION_MESSAGE); 
                    }
                }
                catch(NumberFormatException ex)
                {
                    // Give a large error message about the need for integer width and height
                    JOptionPane.showMessageDialog(null, "Size values must be numbers!", "Bad Input", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }
    }
    
    /**
     * This function creates a JOptionPane with two input fields
     * @param title The title of the pane
     * @param lable1 The first input's label
     * @param lable2 The second input's label
     * @return A string array of length 2 with the 1st string the first result 
     * and the second string being the second result
     */
    private String[] twoInputMessage(String title, String label1, String label2)
    {
        String[] result = new String[2];
        JTextField field1 = new JTextField();
        JTextField field2 = new JTextField();
        Object[] message = {
            label1 + ":", field1,
            label2 + ":", field2
        };

        int option = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            result[0] = field1.getText();
            result[1] = field2.getText();
        } else {
            result = null;
        }
        return result;
    }

    /**
     * Sets the output based on the users choice
     */
    private void setOutput() {
        String[] outputOptions = { "Local", "Amazon" };
        String selected = (String)JOptionPane.showInputDialog(null, "Please choose a location for saving/loading?",
                "Save/Load Location", JOptionPane.DEFAULT_OPTION, null, outputOptions, "Local");
        if ( selected != null ){//null if the user cancels. 
            switch (selected)
            {
                case "Local":
                    ioStrat = new Json();
                    break;
                case "Amazon":
                    ioStrat = new Amazon();
                    break;
            }
        }
    }
}
