/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drawing;

import IO.ImageDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.awt.Graphics;
import java.awt.Point;

/**
 *
 * @author ammon
 */
@JsonDeserialize(using = ImageDeserializer.class)
public interface Image {
    
    public void draw(Graphics graphics) throws ApplicationException;
    public void setSize(Size size) throws ApplicationException;
    public Size getSize();
    public void setLocation(Point location) throws ApplicationException;
    public Point getLocation();
    public boolean isSelected();
    public void select() throws ApplicationException;
    public void unselect() throws ApplicationException;
    public String getType();
}
