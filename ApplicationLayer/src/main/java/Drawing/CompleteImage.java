/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drawing;

import java.awt.Graphics;
import java.awt.Point;

/**
 *
 * @author ammon
 */
public class CompleteImage implements Image{

    private transient final ImageIntrinsicState intrinsicState;
    private final Image extrinsicState;
    
    public CompleteImage(ImageIntrinsicState intrinsicState,
                            ImageExtrinsicState extrinsicState)
    {
        this.intrinsicState = intrinsicState;
        this.extrinsicState = extrinsicState;
    }
    
    @Override
    public void draw(Graphics graphics) {
        
        if (graphics == null || intrinsicState.getImage() == null)
            return;
        
        graphics.drawImage(intrinsicState.getImage(),
                extrinsicState.getLocation().x,
                extrinsicState.getLocation().y,
                extrinsicState.getSize().getWidth(),
                extrinsicState.getSize().getHeight(),
                null);
        if (extrinsicState.isSelected())
        {
            // TODO: Setup the selection pen color
            graphics.drawRect((int)extrinsicState.getLocation().getX(),
                (int)extrinsicState.getLocation().getY(),
                extrinsicState.getSize().getWidth(),
                extrinsicState.getSize().getHeight());
        }
        
        // TODO: Set it to redraw the location of the draw action handle
    }

    @Override
    public void setSize(Size size) throws ApplicationException {
        extrinsicState.setSize(size);
    }

    @Override
    public Size getSize() {
        return extrinsicState.getSize();
    }

    @Override
    public void setLocation(Point location) throws ApplicationException {
        extrinsicState.setLocation(location);
    }

    @Override
    public Point getLocation() {
        return extrinsicState.getLocation();
    }

    @Override
    public boolean isSelected() {
        return extrinsicState.isSelected();
    }

    @Override
    public void select() throws ApplicationException {
        extrinsicState.select();
    }

    @Override
    public void unselect() throws ApplicationException {
        extrinsicState.unselect();
    }
    
    @Override
    public String getType()
    {
        return extrinsicState.getType();
    }
}
