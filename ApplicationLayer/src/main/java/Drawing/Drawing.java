/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drawing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author ammon
 */
public class Drawing {
    public  transient ImageFactory factory = new ImageFactory();
    private final List<Image> images = new CopyOnWriteArrayList<>();
    private transient Lock myLock = new ReentrantLock();
    
    private Image selectedImage;
    private boolean isDirty;
    private java.awt.Image bgImage = null;
    private String backgroundImageName = null;
    private Color backgroundColor;
    private String name = "Untitled";
    
    public Drawing()
    {
        myLock = new ReentrantLock();
    }
    
    public Drawing(File imageFile, Color color) throws IOException
    {
        if (imageFile != null)
        {
            backgroundImageName = imageFile.getAbsolutePath();
            bgImage = ImageIO.read(imageFile);
        }
        backgroundColor = color;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        myLock.lock();
        try
        {
            this.name = name;
            isDirty = true;
        }
        finally
        {
            myLock.unlock();
        }
    }
    
    public Color getBackgroundColor()
    {
        return backgroundColor;
    }
    
    public void setBackgroundColor(Color newColor)
    {
        myLock.lock();
        try
        {
            backgroundColor = newColor;
            isDirty = true;
        }
        finally
        {
            myLock.unlock();
        }
    }
    
    public String getBackgroundImage()
    {
        return backgroundImageName;
    }
    
    public String getBackgroundImageName()
    {
        return backgroundImageName;
    }
    
    public void setBackgroundImageName(String name) throws IOException
    {
        myLock.lock();
        try
        {
            if (name != null)
            {
                backgroundImageName = name;
                bgImage = ImageIO.read(new File(name));
            }
            else
            {
                backgroundImageName = null;
                bgImage = null;
            }
            isDirty = true;
        }
        finally
        {
            myLock.unlock();
        }
    }
    
    public void setBackgroundImage(File file) throws IOException
    {
        myLock.lock();
        try
        {
            if (file != null)
            {
                backgroundImageName = file.getAbsolutePath();
                bgImage = ImageIO.read(file);
            }
            else
            {
                backgroundImageName = null;
                bgImage = null;
            }
            isDirty = true;
        }
        finally
        {
            myLock.unlock();
        }
    }
    
    public List<Image> getImages()
    {
        return images;
    }
    
    public void select(Image image)
    {
        myLock.lock();
        try
        {
            images.get(images.lastIndexOf(image)).select();
            isDirty = true;
        }
        catch (ApplicationException ex) {
            System.out.println(ex);
        }
        finally
        {
            myLock.unlock();
        }
    }
    
    public List<Image> clear()
    {
        List<Image> temp = new CopyOnWriteArrayList<>();
        myLock.lock();
        try
        {
            for (Image i: images)
            {
                temp.add(i);
            }
            images.clear();
            isDirty = true;
        }
        finally
        {
            myLock.unlock();
        }
        
        return temp;
    }
    
    public void add(Image image)
    {
        if (image != null)
        {
            myLock.lock();
            try
            {
                images.add(image);
                isDirty = true;
            }
            finally
            {
                myLock.unlock();
            }
        }
    }
    
    public void addMultiple(List<Image> imagesToAdd)
    {
        if (imagesToAdd != null && imagesToAdd.size() > 0)
        {
            myLock.lock();
            try
            {
                images.addAll(imagesToAdd);
                isDirty = true;
            }
            finally
            {
                myLock.unlock();
            }
        }
    }
    
    public void removeImage(Image image)
    {
        if (image != null)
        {
            myLock.lock();
            try
            {
                if (selectedImage == image)
                {
                    selectedImage = null;
                }
                images.remove(image);
                isDirty = true;
            }
            finally
            {
                myLock.unlock();
            }
        }
    }
    
    public Image findImageAtPosition(Point location)
    {
        Image result = null;
        myLock.lock();
        try
        {
            for (Image img: images)
            {
                if (location.x >= img.getLocation().x
                        && location.x < img.getLocation().x + img.getSize().getWidth()
                        && location.y >= img.getLocation().y
                        && location.y < img.getLocation().y + img.getSize().getHeight())
                    result = img;
            }
        }
        finally
        {
            myLock.unlock();
        }
        return result;
    }
    
    public List<Image> deselectAll()
    {
        List<Image> selected = new CopyOnWriteArrayList<>();
        myLock.lock();
        try
        {
            // Go through and deselect all images that are selected
            for (Image i: images)
            {
                try {
                    if (i.isSelected())
                    {
                        // Save the list of selected images
                        selected.add(i);
                        i.unselect();
                    }
                } catch (ApplicationException ex) {
                    Logger.getLogger(Drawing.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            isDirty = true;
        }
        finally
        {
            myLock.unlock();
        }
        return selected;
    }
    
    public List<Image> deleteAllSelected()
    {
        List<Image> deleted = new ArrayList<>();
        myLock.lock();
        try
        {
            for (Image i: images)
            {
                if (i.isSelected())
                {
                    deleted.add(i);
                    images.remove(i);
                }
            }
            isDirty = true;
        }
        finally
        {
            myLock.unlock();
        }
        return deleted;
    }
    
    public boolean draw(Graphics graphics)
    {
        boolean didARedraw = false;
        myLock.lock();
        try
        {
            // For some reason my program doesn't work with this if statement
            //if (isDirty)
            //{
                // Draw background image and color
                drawBackground(graphics);
            
                // Draw the images
                for(Image i: images)
                {
                    try {
                        i.draw(graphics);
                    } catch (ApplicationException ex) {
                        Logger.getLogger(Drawing.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                // We drew on the changes so we are no longer dirty
                isDirty = false;
                didARedraw = true;
            //}
        }
        finally
        {
            myLock.unlock();
        }
        return didARedraw;
    }
    
    public void dirty()
    {
        myLock.lock();
        try
        {
            isDirty = true;
        }
        finally
        {
            myLock.unlock();
        }
    }
    
    public boolean isDirty()
    {
        boolean dirty;
        myLock.lock();
        try
        {
            dirty = isDirty;
        }
        finally
        {
            myLock.unlock();
        }
        return dirty;
    }
    
    // Draw the background image
    private void drawBackground(Graphics graphics)
    {
        if (bgImage != null) {
                graphics.drawImage(bgImage, 0, 0, bgImage.getWidth(null), bgImage.getHeight(null), null);
            }
    }
}
