/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drawing;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author ammon
 */
public class ImageFactory {
    private transient final Map<String, ImageIntrinsicState> sharedImages = new ConcurrentHashMap<>();
    
    public CompleteImage getImage(ImageExtrinsicState extrinsic) throws IOException
    {
        ImageIntrinsicState intrinsicState;
        if (sharedImages.containsKey(extrinsic.getType()))
        {
            intrinsicState = sharedImages.get(extrinsic.getType());
        }
        else
        {
            intrinsicState = new ImageIntrinsicState();
            intrinsicState.loadImageFile(extrinsic.getType());
            sharedImages.put(extrinsic.getType(), intrinsicState);
        }
        
        return new CompleteImage(intrinsicState, extrinsic);
    }
    
    public ImageExtrinsicState getExtrinsic(String type)
    {
        ImageExtrinsicState extrin = new ImageExtrinsicState();
        extrin.setType(type);
        return extrin;
    }
    
    public Object[] getPossibleTypes()
    {
        String[] types = { "orange", "apple", "pineapple", "grape", "kiwi", "pear" };
        return types;
        // Eventual way of doing it:
        // return sharedImages.keySet().toArray();
    }
}
