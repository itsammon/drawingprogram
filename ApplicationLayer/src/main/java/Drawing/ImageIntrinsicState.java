/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drawing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author ammon
 */
public class ImageIntrinsicState implements Image{

    private String type;
    private BufferedImage image;
    private BufferedImage toolImage;
    private BufferedImage toolImageSelected;
    private Color selectedBGColor = Color.CYAN;
    
    /**
     * Function to copy an image
     * @param bi The original image to copy
     * @return The resulting copy
     */
    private static BufferedImage deepCopy(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(bi.getRaster().createCompatibleWritableRaster());
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }
    
    public void loadImageFile(String type) throws IOException
    {
        // TODO: Load an image file and setup anything needed.
        try {
            // Load the image from a png file and create two copies
            image = ImageIO.read(new File("../Images/" + type + ".png"));
            this.type = type;
            toolImage = deepCopy(image);
            toolImageSelected = deepCopy(toolImage);
            
            // Change the selected image's background color
            Graphics2D g = toolImageSelected.createGraphics();
            g.setColor(selectedBGColor);
            g.fillRect(0, 0, toolImageSelected.getWidth(), toolImageSelected.getHeight());
            g.drawImage(toolImage, 0, 0, null);
            g.dispose();
        } catch (IOException ex) {
            Logger.getLogger(ImageIntrinsicState.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    public BufferedImage getImage()
    {
        return image;
    }
    
    public BufferedImage getTool()
    {
        return toolImage;
    }
    
    public BufferedImage getToolSelected()
    {
        return toolImageSelected;
    }
    
    public void setBGColor(Color color)
    {
        selectedBGColor = color;
    }
    
    public Color getBGColor()
    {
        return selectedBGColor;
    }
    
    @Override
    public void draw(Graphics graphics) throws ApplicationException {
        throw new ApplicationException("Cannot draw image with only intrinsic state!"); 
    }

    @Override
    public void setSize(Size size) throws ApplicationException {
        throw new ApplicationException("Cannot change the shape of an image with only intrinsic state!"); 
    }

    @Override
    public Size getSize() {
        return new Size();
    }

    @Override
    public void setLocation(Point location) throws ApplicationException {
        throw new ApplicationException("Cannot change the location of an image with only intrinsic state!");
    }

    @Override
    public Point getLocation() {
        return new Point();
    }

    @Override
    public boolean isSelected() {
        return false;
    }

    @Override
    public void select() throws ApplicationException {
        throw new ApplicationException("Cannot change the selected state of an image with only intrinsic state!"); 
    }

    @Override
    public void unselect() throws ApplicationException {
        throw new ApplicationException("Cannot change the selected state of an image with only intrinsic state!"); 
    }
    
    @Override
    public String getType()
    {
        return type;
    }
    
}
