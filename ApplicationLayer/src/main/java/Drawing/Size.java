       /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drawing;

/**
 *
 * @author ammon
 */
public class Size {
    private int width;
    private int height;
    
    public Size()
    {
        width = 100;
        height = 100;
    }
    
    public Size (int w, int h)
    {
        if (w > 0)
        {
            width = w;
        }
        else
        {
            width = 100;
        }
        if (h > 0)
        {
            height = h;
        }
        else
        {
            height = 100;
        }
    }
    
    public void setSize(int w, int h)
    {
        setWidth(w);
        setHeight(h);
    }
    
    public void setWidth(int w)
    {
        if (w > 0)
        {
            width = w;
        }
    }
    
    public int getWidth()
    {
        return width;
    }
    
    public void setHeight(int h)
    {
        if (h > 0)
        {
            height = h;
        }
    }
    
    public int getHeight()
    {
        return height;
    }
}
