/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drawing;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.awt.Graphics;
import java.awt.Point;

/**
 *
 * @author ammon
 */
@JsonDeserialize(using=JsonDeserializer.None.class)
public class ImageExtrinsicState implements Image {
    private boolean selected = false;
    private Point location;
    private Size size;
    private String type;

    public ImageExtrinsicState() {
        location = new Point(0,0);
        size = new Size();
        type = "";
    }

    @Override
    public void draw(Graphics graphics) throws ApplicationException {
        throw new ApplicationException("Unable to draw an image with only extrinsic state."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSize(Size size) {
        this.size = size;
    }

    @Override
    public Size getSize() {
        return size;
    }

    @Override
    public void setLocation(Point location) {
        this.location = location;
    }

    @Override
    public Point getLocation() {
        return location;
    }

    @Override
    public boolean isSelected() {
        return selected;
    }

    @Override
    public void select() {
        selected = true;
    }

    @Override
    public void unselect() {
        selected = false;
    }
    
    public void setType(String type)
    {
        this.type = type;
    }
    
    @Override
    public String getType()
    {
        return type;
    }
    
}
