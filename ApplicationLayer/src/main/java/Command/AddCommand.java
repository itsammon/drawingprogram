/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Drawing.ApplicationException;
import Drawing.Drawing;
import Drawing.Image;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ammon
 */
public class AddCommand implements Command{
    
    private final Image image;
    private final Drawing drawing;
    private List<Image> selected;

    public AddCommand(Drawing drawing, Image image)
    {
        this.image = image;
        this.drawing = drawing;
    }
    
    @Override
    public void execute() {
        try {
            // Deselect the images that are not the image we want
            selected = drawing.deselectAll();
            drawing.add(image);
            image.select();
        } catch (ApplicationException ex) {
            Logger.getLogger(AddCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void undo() {
            
        try {// Reselect the previously selected
            for (Image i: selected)
            {
                drawing.select(i);
            }
            drawing.removeImage(image);
            image.unselect();
        } catch (ApplicationException ex) {
            Logger.getLogger(AddCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
