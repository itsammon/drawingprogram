/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Drawing.Drawing;
import Drawing.Image;
import java.awt.Color;
import java.awt.Point;
import java.io.File;

/**
 *
 * @author ammon
 */
public class CommandFactory {
    private Drawing drawing;
    
    public CommandFactory(Drawing drawing)
    {
        this.drawing = drawing;
    }
    
    public void setDrawing(Drawing drawing)
    {
        this.drawing = drawing;
    }
    
    public Command getCommand(String name)
    {
        Command command = null;
        if (drawing != null)
        {
            switch(name)
            {
                case "Clear":
                    command = new ClearCommand(drawing);
                    break;
                case "RemoveSelected":
                    command = new RemoveSelectedCommand(drawing);
                    break;
                case "DeselectAll":
                    command = new DeselectAllCommand(drawing);
                    break;
                default:
                    command = null;
            }
        }
        return command;
    }
    
    public Command getCommand(String name, File imageFile)
    {
        Command command = null;
        if (drawing != null)
        {
            switch(name)
            {
                case "BackgroundImage":
                    command = new BackgroundImageCommand(drawing, imageFile);
                    break;
                default:
                    command = null;
            }
        }
        return command;
    }
    
    public Command getCommand(String name, Color color)
    {
        Command command = null;
        if (drawing != null)
        {
            switch(name)
            {
                case "BackgroundColor":
                    command = new BackgroundColorCommand(drawing, color);
                    break;
                default:
                    command = null;
            }
        }
        return command;
    }
    
    public Command getCommand(String name, Image image)
    {
        Command command = null;
        if (drawing != null)
        {
            switch(name)
            {
                case "Add":
                    if (image != null)
                    {
                        command = new AddCommand(drawing, image);
                    }
                    break;
                case "SelectOne":
                    if (image != null)
                        command = new SelectOneCommand(drawing, image);
                    break;
                case "Select":
                    if (image != null)
                        command = new Select(drawing, image);
                    break;
                case "Duplicate":
                    command = new DuplicateCommand(drawing, image);
                    break;
                case "Clear":
                    command = new ClearCommand(drawing);
                    break;
                default:
                    command = null;
            }
        }// End if
        return command;
    }
    
    public Command getCommand(String name, Image image, Point sPoint, Point ePoint)
    {
        Command command = null;
        if (drawing != null)
        {
            switch(name)
            {
                case "Add":
                    if (image != null)
                    {
                        command = new AddCommand(drawing, image);
                    }
                    break;
                case "SelectOne":
                    if (image != null)
                        command = new SelectOneCommand(drawing, image);
                    break;
                case "Select":
                    if (image != null)
                        command = new Select(drawing, image);
                    break;
                case "Duplicate":
                    command = new DuplicateCommand(drawing, image);
                    break;
                case "Move":
                    command = new MoveCommand(drawing, image, sPoint, ePoint);
                    break;
                default:
                    command = null;
            }
        }// End if
        return command;
    }
    
    public Command getCommand(String name, Image image, int int1, int int2)
    {
        Command command = null;
        if (drawing != null)
        {
            switch(name)
            {
                case "Resize":
                    if (image != null)
                    {
                        command = new ResizeCommand(drawing, image, int1, int2);
                    }
                    break;
                default:
                    command = null;
            }
        }// End if
        return command;
    }
}
