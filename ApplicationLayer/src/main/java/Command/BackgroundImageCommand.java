/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Drawing.Drawing;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ammon
 */
public class BackgroundImageCommand implements Command{
    private final Drawing drawing;
    private final File imageFile;
    private final String oldImageFile;
    
    public BackgroundImageCommand(Drawing drawing, File imagefile)
    {
        this.drawing = drawing;
        oldImageFile = drawing.getBackgroundImage();
        this.imageFile = imagefile;
    }

    @Override
    public void execute() {
        try {
            drawing.setBackgroundImage(imageFile);
        } catch (IOException ex) {
            Logger.getLogger(BackgroundImageCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void undo() {
        try {
            if (oldImageFile != null)
            {
                drawing.setBackgroundImage(new File(oldImageFile));
            }
            else
            {
                drawing.setBackgroundImage(null);
            }
        } catch (IOException ex) {
            Logger.getLogger(BackgroundImageCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
