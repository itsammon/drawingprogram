/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Drawing.Drawing;
import Drawing.Image;
import java.util.List;

/**
 *
 * @author ammon
 */
public class RemoveSelectedCommand implements Command{

    private final Drawing drawing;
    private List<Image> images;
    
    public RemoveSelectedCommand(Drawing drawing)
    {
        this.drawing = drawing;
    }
    
    @Override
    public void execute() {
        images = drawing.deleteAllSelected();
    }

    @Override
    public void undo() {
        drawing.addMultiple(images);
    }
    
}
