/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Drawing.ApplicationException;
import Drawing.Drawing;
import Drawing.Image;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ammon
 */
public class SelectOneCommand implements Command{
    private final Drawing drawing;
    private final Image image;
    private List<Image> selected;

    public SelectOneCommand(Drawing drawing, Image img)
    {
        this.drawing = drawing;
        this.image = img;
    }
    
    @Override
    public void execute() {
        try {
            // Deselect the images that are not the image we want
            selected = drawing.deselectAll();
            image.select();
        } catch (ApplicationException ex) {
            Logger.getLogger(SelectOneCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void undo() {
        try {
            image.unselect();
            // Reselect the previously selected
            for (Image i: selected)
            {
                drawing.select(i);
            }
        } catch (ApplicationException ex) {
            Logger.getLogger(SelectOneCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
