/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Drawing.ApplicationException;
import Drawing.Drawing;
import Drawing.Image;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ammon
 */
public class Select implements Command{
    private final Drawing drawing;
    private final Image image;
    
    public Select(Drawing drawing, Image image)
    {
        this.drawing = drawing;
        this.image = image;
    }
    
    @Override
    public void execute() {
        try {
            image.select();
        } catch (ApplicationException ex) {
            Logger.getLogger(SelectOneCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void undo() {
        try {
            image.unselect();
        } catch (ApplicationException ex) {
            Logger.getLogger(SelectOneCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
