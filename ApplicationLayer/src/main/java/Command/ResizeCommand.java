/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Drawing.ApplicationException;
import Drawing.Drawing;
import Drawing.Image;
import Drawing.Size;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ammon
 */
public class ResizeCommand implements Command{
    private final Size newSize;
    private final Size oldSize;
    private final Image image;
    private final Drawing drawing;
    
    public ResizeCommand(Drawing drawing, Image image, int width, int height)
    {
        this.drawing = drawing;
        this.image = image;
        oldSize = this.image.getSize();
        newSize = new Size(width, height);
    }
    
    @Override
    public void execute() {
        try {
            image.setSize(newSize);
            drawing.dirty();
        } catch (ApplicationException ex) {
            Logger.getLogger(ResizeCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void undo() {
        try {
            image.setSize(oldSize);
            drawing.dirty();
        } catch (ApplicationException ex) {
            Logger.getLogger(ResizeCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
