/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Drawing.Drawing;
import Drawing.Image;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author ammon
 */
public class DeselectAllCommand implements Command{
    private List<Image> selected;
    private final Drawing drawing;
    
    DeselectAllCommand(Drawing drawing)
    {
        this.drawing = drawing;
    }

    @Override
    public void execute() {
        selected = drawing.deselectAll();
    }

    @Override
    public void undo() {
        for (Image i: selected)
        {
            drawing.select(i);
        }
    }
    
}
