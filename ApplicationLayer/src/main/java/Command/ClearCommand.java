/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Drawing.Drawing;
import Drawing.Image;
import java.util.List;

/**
 *
 * @author ammon
 */
public class ClearCommand implements Command{
    private List<Image> images;
    private final Drawing drawing;
    
    public ClearCommand(Drawing drawing)
    {
        this.drawing = drawing;
    }

    @Override
    public void execute() {
        images = drawing.clear();
    }

    @Override
    public void undo() {
        drawing.addMultiple(images);
    }
    
}
