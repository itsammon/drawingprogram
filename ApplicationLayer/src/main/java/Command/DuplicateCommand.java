/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Drawing.ApplicationException;
import Drawing.Drawing;
import Drawing.Image;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ammon
 */
public class DuplicateCommand implements Command{
    private final Drawing drawing;
    private final Image image;
    private Image newImage;
    private List<Image> selected;
    
    public DuplicateCommand(Drawing drawing, Image image)
    {
        this.drawing = drawing;
        this.image = image;
    }

    @Override
    public void execute() {
        try {
            newImage = drawing.factory.getImage(drawing.factory.getExtrinsic(image.getType()));
            
            newImage.setSize(image.getSize());
            
            // Deselect the images that are not the image we want
            selected = drawing.deselectAll();
            drawing.add(newImage);
            newImage.select();
        } catch (ApplicationException ex) {
            Logger.getLogger(AddCommand.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DuplicateCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void undo() {
        try {
            // Reselect the previously selected
            for (Image i: selected)
            {
                drawing.select(i);
            }
            drawing.removeImage(newImage);
            newImage.unselect();
        } catch (ApplicationException ex) {
            Logger.getLogger(AddCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
