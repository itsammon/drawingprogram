/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Drawing.Drawing;
import java.awt.Color;

/**
 *
 * @author ammon
 */
public class BackgroundColorCommand implements Command{
    private final Drawing drawing;
    private final Color oldColor;
    private final Color color;
    
    public BackgroundColorCommand(Drawing drawing, Color color)
    {
        this.drawing = drawing;
        oldColor = drawing.getBackgroundColor();
        this.color = color;
    }

    @Override
    public void execute() {
        drawing.setBackgroundColor(color);
    }

    @Override
    public void undo() {
        drawing.setBackgroundColor(oldColor);
    }
}
