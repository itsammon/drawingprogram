/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Drawing.ApplicationException;
import Drawing.Drawing;
import Drawing.Image;
import java.awt.Point;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ammon
 */
public class MoveCommand implements Command{
    private Point startPosition;
    private Point endPosition;
    private Image image;
    private Drawing drawing;
    
    public MoveCommand(Drawing drawing, Image image, Point startPoint, Point endPoint)
    {
        this.drawing = drawing;
        this.image = image;
        startPosition = startPoint;
        endPosition = endPoint;
    }
    
    @Override
    public void execute() {
        try {
            image.setLocation(endPosition);
            drawing.dirty();
        } catch (ApplicationException ex) {
            Logger.getLogger(MoveCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void undo() {
        try {
            image.setLocation(startPosition);
            drawing.dirty();
        } catch (ApplicationException ex) {
            Logger.getLogger(MoveCommand.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
}
