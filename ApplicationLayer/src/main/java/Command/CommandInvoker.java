/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ammon
 */
public class CommandInvoker implements Runnable{
    private final ConcurrentLinkedQueue<Command> commands = new ConcurrentLinkedQueue<>();
    private final ConcurrentLinkedDeque<Command> undoStack = new ConcurrentLinkedDeque<>();
    private final ConcurrentLinkedDeque<Command> redoStack = new ConcurrentLinkedDeque<>();
    private boolean keepGoing;
    private Thread worker;
    private final AutoResetEvent eventAdded = new AutoResetEvent(false);
    
    public void start()
    {
        keepGoing = true;
        worker = new Thread(this);
        worker.start();
    }
    
    public void stop()
    {
        keepGoing = false;
    }
    
    public void enqueueCommand(Command cmd)
    {
        if (cmd != null)
        {
            commands.add(cmd);
            eventAdded.set();
        }
    }
    
    public void undo()
    {
        if (!undoStack.isEmpty())
        {
            Command cmd = undoStack.pop();
            if (cmd != null)
            {
                cmd.undo();
                redoStack.push(cmd);
            }
        }
    }
    
    public void redo()
    {
        if (!redoStack.isEmpty())
        {
            Command cmd = redoStack.pop();
            if (cmd != null)
            {
                enqueueCommand(cmd);
            }
        }
    }
    
    @Override
    public void run() {
        Command command;
        while (keepGoing)
        {
            // TODO: Try to get a command out of the queue
            // Execute command
            // Add command to undo stack
            command = commands.poll();
            if (command != null)
            {
                command.execute();
                undoStack.push(command);
            }
            
            try {
                eventAdded.waitOne(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(CommandInvoker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}

/**
 * Class to mimic the AutoResetEvent from .NET
 * From http://stackoverflow.com/questions/1091973/javas-equivalent-to-nets-autoresetevent
 */
class AutoResetEvent
{
    private final Object _monitor = new Object();
    private volatile boolean _isOpen = false;

    public AutoResetEvent(boolean open)
    {
        _isOpen = open;
    }

    public void waitOne() throws InterruptedException
    {
        synchronized (_monitor) {
            while (!_isOpen) {
                _monitor.wait();
            }
            _isOpen = false;
        }
    }

    public void waitOne(long timeout) throws InterruptedException
    {
        synchronized (_monitor) {
            long t = System.currentTimeMillis();
            while (!_isOpen) {
                _monitor.wait(timeout);
                // Check for timeout
                if (System.currentTimeMillis() - t >= timeout)
                    break;
            }
            _isOpen = false;
        }
    }

    public void set()
    {
        synchronized (_monitor) {
            _isOpen = true;
            _monitor.notify();
        }
    }

    public void reset()
    {
        _isOpen = false;
    }
}
