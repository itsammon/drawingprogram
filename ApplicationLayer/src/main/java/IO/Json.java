/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IO;

import Drawing.Drawing;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author ammon
 */
public class Json implements IOStrategy{

    @Override
    public void save(Drawing drawing, String filename) throws FileNotFoundException, IOException {
        ObjectMapper mapper = new ObjectMapper();	
        // Allow conversion of colors
        final SimpleModule module = new SimpleModule();
        module.setMixInAnnotation(Color.class, ColorMixIn.class);
        mapper.registerModule(module);
      
        mapper.writeValue(new File(filename+".json"), drawing);
    }

    @Override
    public Drawing load(String filename) throws FileNotFoundException, IOException {
        String line;
        String json = "";
        Drawing drawing = null;
        ObjectMapper mapper = new ObjectMapper();
        
        // Allow conversion of colors
        final SimpleModule module = new SimpleModule();
        module.setMixInAnnotation(Color.class, ColorMixIn.class);
        mapper.registerModule(module);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        
        try (BufferedReader read = new BufferedReader(new FileReader(filename)))
        {
            while ((line = read.readLine()) != null)
            {
                json += line;
            }
            System.out.println(mapper.readValue(json, Drawing.class));
            drawing = mapper.readValue(json, Drawing.class);
            read.close();
        }
        catch (JsonParseException e) { e.printStackTrace();}

        return drawing;
    }
    
    // Class to handle colors
    @JsonAutoDetect(
            fieldVisibility = JsonAutoDetect.Visibility.NONE,
            isGetterVisibility = JsonAutoDetect.Visibility.NONE,
            getterVisibility = JsonAutoDetect.Visibility.NONE,
            setterVisibility = JsonAutoDetect.Visibility.NONE)
    public static abstract class ColorMixIn extends Color {

        public ColorMixIn(
                @JsonProperty("red") int red,
                @JsonProperty("green") int green,
                @JsonProperty("blue") int blue,
                @JsonProperty("alpha") int alpha) {
            super(red, green, blue, alpha);
        }
        @JsonProperty("red") public abstract int getRed();
        @JsonProperty("green") public abstract int getGreen();
        @JsonProperty("blue") public abstract int getBlue();
        @JsonProperty("alpha") public abstract int getAlpha();
    }
}


