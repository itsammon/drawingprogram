/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IO;

import Drawing.Drawing;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author ammon
 */
public interface IOStrategy {
    
    public void save(Drawing drawing, String filename) throws FileNotFoundException, IOException;
    public Drawing load(String filename) throws FileNotFoundException, IOException;
}
