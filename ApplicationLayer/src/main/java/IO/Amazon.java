/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IO;

import Drawing.Drawing;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;

/**
 *
 * @author ammon
 */
public class Amazon implements IOStrategy {
    private static final String BUCKET = "ammon.drawings";

    @Override
    public void save(Drawing drawing, String filename) throws FileNotFoundException, IOException {
        try {
            AmazonS3 s3client = new AmazonS3Client(new ProfileCredentialsProvider());
            
            ObjectMapper mapper = new ObjectMapper();	
            // Allow conversion of colors
            final SimpleModule module = new SimpleModule();
            module.setMixInAnnotation(Color.class, Json.ColorMixIn.class);
            mapper.registerModule(module);
            mapper.writeValue(new File(filename+".json"), drawing);
                  
            // Temporary file
            File file = new File(filename+".json");
            
            s3client.putObject(new PutObjectRequest(BUCKET, filename + ".json", file));
        
            // Cleanup the temp file
            Files.deleteIfExists(file.toPath());
        }
        catch(AmazonServiceException ase)
        {
            System.out.println("Error message: " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
        }
        catch(AmazonClientException ace)
        {
            System.out.println("Error Meassage: " + ace.getMessage());
        }
    }

    @Override
    public Drawing load(String filename) throws FileNotFoundException, IOException {
        
        // TODO: Fix JSON loading
        String line;
        String json = "";
        Drawing drawing = null;
        ObjectMapper mapper = new ObjectMapper();
      
        filename = filename + ".json";
        try
        {
            AmazonS3 s3client = new AmazonS3Client(new ProfileCredentialsProvider());
            
            S3Object object = s3client.getObject(
                  new GetObjectRequest(BUCKET, filename));
            InputStream objectData = object.getObjectContent();

            // Allow conversion of colors
            final SimpleModule module = new SimpleModule();
            module.setMixInAnnotation(Color.class, Json.ColorMixIn.class);
            mapper.registerModule(module);
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        
            BufferedReader read = new BufferedReader(new InputStreamReader(objectData));
                while ((line = read.readLine()) != null)
                {
                    json += line;
                }
            drawing = mapper.readValue(json, Drawing.class);
            read.close();
        }
        catch (AmazonServiceException e) {
            String errorCode = e.getErrorCode();
                // If the file does not exist, we can handle it.
                if (errorCode.equals("NoSuchKey")) {
                    throw new FileNotFoundException("File does not exist!");
                }
                else
                {
                    // If something else went wrong, end the thread.
                    throw e;
                }
        }
        catch (JsonParseException e) { throw new IOException("Error parsing file!"); }

        return drawing;
    }
    
    // Class to handle colors
    @JsonAutoDetect(
            fieldVisibility = JsonAutoDetect.Visibility.NONE,
            isGetterVisibility = JsonAutoDetect.Visibility.NONE,
            getterVisibility = JsonAutoDetect.Visibility.NONE,
            setterVisibility = JsonAutoDetect.Visibility.NONE)
    public static abstract class ColorMixIn extends Color {

        public ColorMixIn(
                @JsonProperty("red") int red,
                @JsonProperty("green") int green,
                @JsonProperty("blue") int blue,
                @JsonProperty("alpha") int alpha) {
            super(red, green, blue, alpha);
        }
        @JsonProperty("red") public abstract int getRed();
        @JsonProperty("green") public abstract int getGreen();
        @JsonProperty("blue") public abstract int getBlue();
        @JsonProperty("alpha") public abstract int getAlpha();
    }
}
