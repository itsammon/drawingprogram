/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IO;

import Drawing.Image;
import Drawing.ImageExtrinsicState;
import Drawing.ImageFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;

/**
 *
 * @author ammon
 */
public class ImageDeserializer extends StdDeserializer<Image>{
    private static ImageFactory factory;
    
    public ImageDeserializer() {
        this(null);
        factory = new ImageFactory();
    }
    
    public ImageDeserializer(Class<?> vc) {
        super(vc);
        factory = new ImageFactory();
    }
    
    @Override
    public Image deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
        ImageExtrinsicState extrin = jp.getCodec().treeToValue(node, ImageExtrinsicState.class);
        Image image = factory.getImage(extrin);
        
        return image;
    }
    
}
