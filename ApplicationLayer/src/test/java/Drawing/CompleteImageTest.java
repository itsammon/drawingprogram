/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drawing;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class CompleteImageTest {
    private static ImageIntrinsicState intrinsic;
    private static ImageExtrinsicState extrinsic;
    private static CompleteImage instance;
    private static final String type = "orange";
    
    public CompleteImageTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        try {
            intrinsic = new ImageIntrinsicState();
            intrinsic.loadImageFile(type);
            extrinsic = new ImageExtrinsicState();
            extrinsic.setType(type);
            
            instance = new CompleteImage(intrinsic, extrinsic);
            
        } catch (IOException ex) {
            fail("Unable to load test image file!");
        }
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of draw method, of class CompleteImage.
     */
    /*
    @Test
    public void testDraw() {
        System.out.println("draw");
        Graphics graphics = new BufferedImage(10,10,BufferedImage.TYPE_INT_ARGB).getGraphics();
        CompleteImage instance = null;
        instance.draw(graphics);
        // TODO review the generated test code and remove the default call to fail.
        // TODO find best way to test draw function
        fail("The test case is a prototype.");
    }
    */

    /**
     * Test of setSize method, of class CompleteImage.
     */
    @Test
    public void testSetSize() {
        try {
            System.out.println("setSize");
            Size size = new Size(10,10);
            instance.setSize(size);
            assertEquals(size.getHeight(), instance.getSize().getHeight());
            assertEquals(size.getWidth(), instance.getSize().getWidth());
        } catch (ApplicationException ex) {
            fail("Tried to alter internall state");
        }
    }

    /**
     * Test of getSize method, of class CompleteImage.
     */
    @Test
    public void testGetSize() {
        try {
            System.out.println("getSize");
            Size expResult = new Size(10,10);
            instance.setSize(expResult);
            Size result = instance.getSize();
            assertEquals(expResult.getHeight(), instance.getSize().getHeight());
            assertEquals(expResult.getWidth(), instance.getSize().getWidth());
        } catch (ApplicationException ex) {
            Logger.getLogger(CompleteImageTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of setLocation method, of class CompleteImage.
     */
    @Test
    public void testSetLocation() {
        try {
            System.out.println("setLocation");
            Point location = new Point(50, 50);
            instance.setLocation(location);
            assertEquals(location, instance.getLocation());
        } catch (ApplicationException ex) {
            fail("Tried to change intrinsic state!");
        }
    }

    /**
     * Test of getLocation method, of class CompleteImage.
     */
    @Test
    public void testGetLocation() {
        try {
            System.out.println("getLocation");
            Point expResult = new Point(0,0);
            instance.setLocation(expResult);
            Point result = instance.getLocation();
            assertEquals(expResult, result);
        } catch (ApplicationException ex) {
            Logger.getLogger(CompleteImageTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of isSelected method, of class CompleteImage.
     */
    @Test
    public void testIsSelected() {
        System.out.println("isSelected");
        boolean expResult = false;
        boolean result = instance.isSelected();
        assertEquals(expResult, result);
    }

    /**
     * Test of select method, of class CompleteImage.
     */
    @Test
    public void testSelect() {
        try {
            System.out.println("select");
            boolean expResult = true;
            instance.select();
            assertEquals(expResult, instance.isSelected());
        } catch (ApplicationException ex) {
            fail("Tried to select intrinsic state!");
        }
    }

    /**
     * Test of unselect method, of class CompleteImage.
     */
    @Test
    public void testUnselect() {
        try {
            System.out.println("unselect");
            boolean expResult = false;
            instance.unselect();
            assertEquals(expResult, instance.isSelected());
        } catch (ApplicationException ex) {
            fail("Tried to select intrinsic state!");
        }
    }
    
}
