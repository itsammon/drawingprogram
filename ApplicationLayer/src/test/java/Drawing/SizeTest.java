/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drawing;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class SizeTest {
    
    public SizeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setSize method, of class Size.
     */
    @Test
    public void testSetSize() {
        System.out.println("setSize");
        int w = 50;
        int h = 50;
        Size instance = new Size();
        instance.setSize(w, h);
        assertEquals(instance.getHeight(), h);
        assertEquals(instance.getWidth(), w);
    }

    /**
     * Test of setWidth method, of class Size.
     */
    @Test
    public void testSetWidth() {
        System.out.println("setWidth");
        int w = 50;
        Size instance = new Size();
        instance.setWidth(w);
        assertEquals(instance.getWidth(), w);
    }
    
    /**
     * Test of setWidth method, of class Size.
     */
    @Test
    public void testSetWidth0() {
        System.out.println("setWidth0");
        int w = 0;
        Size instance = new Size();
        instance.setWidth(w);
        if (instance.getWidth() == 0)
            fail("The width should not be 0.");
    }
    
    /**
     * Test of setWidth method, of class Size.
     */
    @Test
    public void testSetWidthLessThan0() {
        System.out.println("setWidthLessThan");
        int w = -1;
        Size instance = new Size();
        instance.setWidth(w);
        if (instance.getWidth() == w)
            fail("The width should not be less than 0.");
    }

    /**
     * Test of setHeight method, of class Size.
     */
    @Test
    public void testSetHeight() {
        System.out.println("setHeight");
        int h = 50;
        Size instance = new Size();
        instance.setHeight(h);
        assertEquals(instance.getHeight(), h);
    }
    
    /**
     * Test of setWidth method, of class Size.
     */
    @Test
    public void testSetHeight0() {
        System.out.println("setHeight0");
        int h = 0;
        Size instance = new Size();
        instance.setHeight(h);
        if (instance.getHeight() == 0)
            fail("The height should not be 0.");
    }
    
    /**
     * Test of setWidth method, of class Size.
     */
    @Test
    public void testSetHeightLessThan0() {
        System.out.println("setHeightLessThan0");
        int h = -1;
        Size instance = new Size();
        instance.setHeight(h);
        if (instance.getHeight() == h)
            fail("The height should not be less than 0.");
    }
}
