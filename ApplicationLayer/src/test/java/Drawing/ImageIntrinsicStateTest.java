/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drawing;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class ImageIntrinsicStateTest {
    private static ImageIntrinsicState instance;
    private static String image;
    private static Color testColor;
    
    public ImageIntrinsicStateTest() {
    }
    
    public static boolean compareImages(BufferedImage imgA, BufferedImage imgB) {
        // The images must be the same size.
        if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
            int width = imgA.getWidth();
            int height = imgA.getHeight();

            // Loop over every pixel.
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    // Compare the pixels for equality.
                    if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
                        return false;
                    }
                }
            }
        } else {
            return false;
        }

        return true;
    }
    
    @BeforeClass
    public static void setUpClass() {
        image = "orange";
        testColor = Color.CYAN;
        instance = new ImageIntrinsicState();
        try
        {
            instance.loadImageFile(image);
            instance.setBGColor(testColor);
        } catch (IOException ex) {
            fail("Failed to load the test image!");
        }
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of loadImageFile method, of class ImageIntrinsicState.
     */
    @Test
    public void testLoadImageFile() throws Exception {
        System.out.println("loadImageFile");
        String type = "orange";
        ImageIntrinsicState testInstance = new ImageIntrinsicState();
        testInstance.loadImageFile(type);
        assertEquals(type, testInstance.getType());
    }

    /**
     * Test of getImage method, of class ImageIntrinsicState.
     */
    @Test
    public void testGetImage() {
        try {
            System.out.println("getImage");
            BufferedImage expResult = ImageIO.read(new File("../Images/"+image+".png"));
            BufferedImage result = instance.getImage();
            assertTrue(compareImages(expResult, result));
        } catch (IOException ex) {
            fail("Unable to load test image!");
        }
    }

    /**
     * Test of getTool method, of class ImageIntrinsicState.
     */
    @Test
    public void testGetTool() {
        try {
            System.out.println("getTool");
            BufferedImage expResult = ImageIO.read(new File("../Images/"+image+".png"));
            BufferedImage result = instance.getTool();
            assertTrue(compareImages(result, expResult));
        } catch (IOException ex) {
            fail("Unable to load test image!");
        }
    }

    /**
     * Test of getToolSelected method, of class ImageIntrinsicState.
     */
    /*
    @Test
    public void testGetToolSelected() {
        try {
            System.out.println("getToolSelected");
            BufferedImage expResult = ImageIO.read(new File("../Images/"+image+".png"));
            Graphics2D g = expResult.createGraphics();
            g.setColor(testColor);
            g.fillRect(0, 0, expResult.getWidth(), expResult.getHeight());
            g.drawImage(expResult, 0, 0, null);
            g.dispose();
            BufferedImage result = instance.getToolSelected();
            assertTrue(compareImages(result, expResult));
        } catch (IOException ex) {
            fail("Unable to load test image!");
        }
    }
    */

    /**
     * Test of setBGColor method, of class ImageIntrinsicState.
     */
    @Test
    public void testSetBGColor() {
        System.out.println("setBGColor");
        Color color = Color.GREEN;
        ImageIntrinsicState instance = new ImageIntrinsicState();
        instance.setBGColor(color);
        assertEquals(instance.getBGColor(), color);
    }

    /**
     * Test of getBGColor method, of class ImageIntrinsicState.
     */
    @Test
    public void testGetBGColor() {
        System.out.println("getBGColor");
        Color expResult = testColor;
        Color result = instance.getBGColor();
        assertEquals(expResult, result);
    }

    /**
     * Test of draw method, of class ImageIntrinsicState.
     */
    @Test
    public void testDraw() {
        System.out.println("draw");
        Graphics graphics = instance.getImage().getGraphics();
        try
        {
            instance.draw(graphics);
            fail("The image should not be able to be drawn.");
        }
        catch (ApplicationException ex)
        {
            assertTrue(true);
        }
    }

    /**
     * Test of setSize method, of class ImageIntrinsicState.
     */
    @Test
    public void testSetSize() {
        System.out.println("setSize");
        // Since size class is tested by itself, I don't need to test for negative sizes
        Size size = new Size(10,10);
        try {
            instance.setSize(size);
            fail("Intrinsic state should not be able to be changed!");
        } catch (ApplicationException ex) {
            assertTrue(true);
        }
    }

    /**
     * Test of getSize method, of class ImageIntrinsicState.
     */
    @Test
    public void testGetSize() {
        System.out.println("getSize");
        Size expResult = new Size(100,100);
        Size result = instance.getSize();
        assertEquals(expResult.getHeight(), result.getHeight());
        assertEquals(expResult.getWidth(), result.getWidth());
    }

    /**
     * Test of setLocation method, of class ImageIntrinsicState.
     */
    @Test
    public void testSetLocation() {
        System.out.println("setLocation");
        Point location = new Point(50,50);
        try {
            instance.setLocation(location);
            fail("Intrinsic state should not be able to be changed!");
        } catch (ApplicationException ex) {
            assertTrue(true);
        }
    }

    /**
     * Test of getLocation method, of class ImageIntrinsicState.
     */
    @Test
    public void testGetLocation() {
        System.out.println("getLocation");
        Point expResult = new Point();
        Point result = instance.getLocation();
        assertEquals(expResult, result);
    }

    /**
     * Test of isSelected method, of class ImageIntrinsicState.
     */
    @Test
    public void testIsSelected() {
        System.out.println("isSelected");
        boolean expResult = false;
        boolean result = instance.isSelected();
        assertEquals(expResult, result);
    }

    /**
     * Test of select method, of class ImageIntrinsicState.
     */
    @Test
    public void testSelect() {
        System.out.println("select");
        try {
            instance.select();
            fail("Intrinsic state should not be selectable!");
        } catch (ApplicationException ex) {
            assertTrue(true);
        }
    }

    /**
     * Test of unselect method, of class ImageIntrinsicState.
     */
    @Test
    public void testUnselect() {
        System.out.println("unselect");
        try {
            instance.unselect();
            fail("Intrinsic state should not be selectable!");
        } catch (ApplicationException ex) {
            assertTrue(true);
        }
    }

    /**
     * Test of getType method, of class ImageIntrinsicState.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        String expResult = "orange";
        String result = instance.getType();
        assertEquals(expResult, result);
    }
    
}
