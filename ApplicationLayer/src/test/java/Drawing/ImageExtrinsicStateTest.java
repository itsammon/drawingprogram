/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drawing;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class ImageExtrinsicStateTest {
    private static ImageExtrinsicState instance;
    
    public ImageExtrinsicStateTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new ImageExtrinsicState();
        instance.unselect();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of draw method, of class ImageExtrinsicState.
     */
    @Test
    public void testDraw() {
        System.out.println("draw");
        Graphics graphics = new BufferedImage(10,10,BufferedImage.TYPE_INT_ARGB).getGraphics();
        try
        {
            instance.draw(graphics);
            fail("The image should not be able to be drawn.");
        }
        catch (ApplicationException ex)
        {
            assertTrue(true);
        }
    }

    /**
     * Test of setSize method, of class ImageExtrinsicState.
     */
    @Test
    public void testSetSize() {
        System.out.println("setSize");
        Size size = new Size(10,10);
        ImageExtrinsicState testInstance = new ImageExtrinsicState();
        testInstance.setSize(size);
        assertEquals(size.getHeight(), testInstance.getSize().getHeight());
        assertEquals(size.getWidth(), testInstance.getSize().getWidth());
    }

    /**
     * Test of getSize method, of class ImageIntrinsicState.
     */
    @Test
    public void testGetSize() {
        System.out.println("getSize");
        Size expResult = new Size(100,100);
        Size result = instance.getSize();
        assertEquals(expResult.getHeight(), result.getHeight());
        assertEquals(expResult.getWidth(), result.getWidth());
    }

    /**
     * Test of setLocation method, of class ImageExtrinsicState.
     */
    @Test
    public void testSetLocation() {
        System.out.println("setLocation");
        Point location = new Point(50,50);
        ImageExtrinsicState testInstance = new ImageExtrinsicState();
        testInstance.setLocation(location);
        assertEquals(location, testInstance.getLocation());
    }
    
    /**
     * Test of setLocation method, of class ImageExtrinsicState.
     */
    @Test
    public void testSetLocation2() {
        System.out.println("setLocation2");
        Point location = new Point(-50,-50);
        ImageExtrinsicState testInstance = new ImageExtrinsicState();
        testInstance.setLocation(location);
        assertEquals(location, testInstance.getLocation());
    }

    /**
     * Test of getLocation method, of class ImageExtrinsicState.
     */
    @Test
    public void testGetLocation() {
        System.out.println("getLocation");
        Point expResult = new Point(50,50);
        instance.setLocation(expResult);
        Point result = instance.getLocation();
        assertEquals(expResult, result);
    }

    /**
     * Test of isSelected method, of class ImageExtrinsicState.
     */
    @Test
    public void testIsSelected() {
        System.out.println("isSelected");
        boolean expResult = false;
        boolean result = instance.isSelected();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isSelected method, of class ImageExtrinsicState.
     */
    @Test
    public void testIsSelected2() {
        System.out.println("isSelected2");
        boolean expResult = true;
        boolean result = instance.isSelected();
        assertEquals(!expResult, result);
    }

    /**
     * Test of select method, of class ImageExtrinsicState.
     */
    @Test
    public void testSelect() {
        System.out.println("select");
        boolean expResult = true;
        instance.select();
        assertEquals(expResult, instance.isSelected());
    }

    /**
     * Test of unselect method, of class ImageExtrinsicState.
     */
    @Test
    public void testUnselect() {
        System.out.println("unselect");
        boolean expResult = false;
        instance.unselect();
        assertEquals(expResult, instance.isSelected());
    }

    /**
     * Test of setType method, of class ImageExtrinsicState.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        String type = "orange";
        instance.setType(type);
        assertEquals(type, instance.getType());
    }

    /**
     * Test of getType method, of class ImageExtrinsicState.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        String expResult = "orange";
        String result = instance.getType();
        assertEquals(expResult, result);
    }
    
}
