/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drawing;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class ImageFactoryTest {
    
    public ImageFactoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getImage method, of class ImageFactory.
     */
    @Test
    public void testGetImage() {
        try {
            System.out.println("getImage");
            ImageExtrinsicState extrinsic = new ImageExtrinsicState();
            extrinsic.setType("orange");
            ImageFactory instance = new ImageFactory();
            CompleteImage result = instance.getImage(extrinsic);
            assertNotNull(result);
        } catch (IOException ex) {
            fail("Unable to load test file!");
        }
    }
    
    /**
     * Test of getImage method, of class ImageFactory.
     */
    @Test
    public void testGetImageFail() {
        try {
            System.out.println("getImageFail");
            ImageExtrinsicState extrinsic = new ImageExtrinsicState();
            extrinsic.setType("b");
            ImageFactory instance = new ImageFactory();
            CompleteImage result = instance.getImage(extrinsic);
            assertNull(result);
        } catch (IOException ex) {
            assertTrue(true);
        }
    }

    /**
     * Test of getExtrinsic method, of class ImageFactory.
     */
    @Test
    public void testGetExtrinsic() {
        System.out.println("getExtrinsic");
        String type = "orange";
        ImageFactory instance = new ImageFactory();
        ImageExtrinsicState expResult = new ImageExtrinsicState();
        expResult.setType(type);
        ImageExtrinsicState result = instance.getExtrinsic(type);
        assertEquals(expResult.getType(), result.getType());
    }
}
