/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Drawing;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class DrawingTest {
    
    public DrawingTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of clear method, of class Drawing.
     */
    @Test
    public void testClear() {
        try {
            System.out.println("clear");
            Drawing instance = new Drawing(null, null);
            instance.clear();
            assertTrue(instance.getImages().isEmpty());
        } catch (IOException ex) {
            Logger.getLogger(DrawingTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of add method, of class Drawing.
     */
    @Test
    public void testAdd() {
        try {
            System.out.println("add");
            Drawing instance = new Drawing(null, null);
            instance.factory = new ImageFactory();
            ImageExtrinsicState extrin = new ImageExtrinsicState();
            extrin.setType("orange");
            Image image = instance.factory.getImage(extrin);
            instance.add(image);
            assertTrue(instance.getImages().contains(image));
        } catch (IOException ex) {
            fail("Unable to load test file!");
        }
    }

    /**
     * Test of addMultiple method, of class Drawing.
     */
    @Test
    public void testAddMultiple() {
        System.out.println("addMultiple");
        Drawing instance;
        try {
            instance = new Drawing(null, Color.WHITE);
            instance.factory = new ImageFactory();
            List<Image> imagesToAdd = new ArrayList<>();
            ImageExtrinsicState extrin;
            for (int i = 0; i < 10; ++i) {
                try {
                    extrin = new ImageExtrinsicState();
                    extrin.setType("orange");
                    Image image = instance.factory.getImage(extrin);
                    imagesToAdd.add(image);
                } catch (IOException ex) {
                    fail("Unable to load image test file!");
                }
            }
            instance.addMultiple(imagesToAdd);
            assertTrue(instance.getImages().containsAll(imagesToAdd));
        } catch (IOException ex) {
            Logger.getLogger(DrawingTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of removeImage method, of class Drawing.
     */
    @Test
    public void testRemoveImage() {
        try {
            System.out.println("removeImage");
            Drawing instance = new Drawing(null,null);
            instance.factory = new ImageFactory();
            ImageExtrinsicState extrin = new ImageExtrinsicState();
            extrin.setType("orange");
            Image image = instance.factory.getImage(extrin);
            instance.add(image);
            instance.removeImage(image);
            assertFalse(instance.getImages().contains(image));
        } catch (IOException ex) {
            fail("Unable to load image test file!");
        }
    }

    /**
     * Test of deselectAll method, of class Drawing.
     */
    @Test
    public void testDeselectAll() {
        try {
            System.out.println("deselectAll");
            Drawing instance = new Drawing(null, null);
            instance.factory = new ImageFactory();
            ImageExtrinsicState extrin = new ImageExtrinsicState();
            extrin.setType("orange");
            Image image = instance.factory.getImage(extrin);
            instance.add(image);
            instance.select(image);
            instance.deselectAll();
            for(Image i: instance.getImages())
            {
                assertFalse(i.isSelected());
            }
        } catch (IOException ex) {
            fail("Unable to load image test file!");
        }
    }

    /**
     * Test of deleteAllSelected method, of class Drawing.
     */
    @Test
    public void testDeleteAllSelected() {
        try {
            System.out.println("deleteAllSelected");
            Drawing instance = new Drawing(null, null);
            instance.factory = new ImageFactory();
            List<Image> imagesToAdd = new ArrayList<>();
            List<Image> imagesToDelete = new ArrayList<>();
            List<Image> expResult = new ArrayList<>();
            ImageExtrinsicState extrin;
            for (int i = 0; i < 10; ++i)
            {
                try {
                    extrin = new ImageExtrinsicState();
                    extrin.setType("orange");
                    Image image = instance.factory.getImage(extrin);
                    imagesToAdd.add(image);
                    if (i % 2 == 0)
                    {
                        imagesToDelete.add(image);
                    }
                    else
                    {
                        expResult.add(image);
                    }
                } catch (IOException ex) {
                    fail("Unable to load image test file!");
                }
            }
            instance.addMultiple(imagesToAdd);
            for (Image i: imagesToDelete)
            {
                instance.select(i);
            }
            List<Image> result = instance.deleteAllSelected();
            assertEquals(imagesToDelete, result);
            assertEquals(expResult, instance.getImages());
        } catch (IOException ex) {
            Logger.getLogger(DrawingTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test of draw method, of class Drawing.
     */
    // TODO: Finish and fix draw tests
    /*
    @Test
    public void testDraw() {
        System.out.println("draw");
        Graphics graphics = null;
        Drawing instance = new Drawing();
        boolean expResult = false;
        boolean result = instance.draw(graphics);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    */
    
}
