/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Drawing.Drawing;
import java.awt.Color;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class BackgroundColorCommandTest {
    private static Drawing drawing;
    private static BackgroundColorCommand instance;
    
    public BackgroundColorCommandTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        try {
            drawing = new Drawing(null, Color.WHITE);
            instance = new BackgroundColorCommand(drawing, Color.BLACK);
        } catch (IOException ex) {
            Logger.getLogger(BackgroundColorCommandTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of execute method, of class BackgroundColorCommand.
     */
    @Test
    public void testExecute() {
        System.out.println("execute");
        instance.execute();
        assertEquals(drawing.getBackgroundColor(), Color.BLACK);
    }

    /**
     * Test of undo method, of class BackgroundColorCommand.
     */
    @Test
    public void testUndo() {
        System.out.println("undo");
        instance.undo();
        assertEquals(drawing.getBackgroundColor(), Color.WHITE);
    }
    
}
