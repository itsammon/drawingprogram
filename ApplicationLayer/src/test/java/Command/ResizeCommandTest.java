/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Drawing.ApplicationException;
import Drawing.Drawing;
import Drawing.Image;
import Drawing.ImageExtrinsicState;
import Drawing.ImageFactory;
import Drawing.Size;
import java.awt.Color;
import java.awt.Point;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class ResizeCommandTest {
    private static Drawing drawing;
    private static ResizeCommand instance;
    private static Image image;
    private static Size startSize;
    private static Size endSize;
    
    public ResizeCommandTest() {
        AddCommand add;
        try {
            drawing = new Drawing(null, Color.WHITE);
            drawing.factory = new ImageFactory();
            ImageExtrinsicState extrin = new ImageExtrinsicState();
            extrin.setType("orange");
            image = drawing.factory.getImage(extrin);
            add = new AddCommand(drawing, image);
            startSize = new Size(50,50);
            endSize = new Size(200,200);
            image.select();
            image.setSize(startSize);
            add.execute();
            instance = new ResizeCommand(drawing, image, endSize.getWidth(), endSize.getHeight());
        } catch (IOException ex) {
            Logger.getLogger(AddCommandTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ApplicationException ex) {
            Logger.getLogger(DeselectAllCommandTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of execute method, of class ResizeCommand.
     */
    @Test
    public void testExecute() {
        System.out.println("execute");
        instance.execute();
        assertEquals(endSize.getWidth(), image.getSize().getWidth());
        assertEquals(endSize.getHeight(), image.getSize().getHeight());
    }

    /**
     * Test of undo method, of class ResizeCommand.
     */
    @Test
    public void testUndo() {
        System.out.println("undo");
        instance.undo();
        assertEquals(startSize.getWidth(), image.getSize().getWidth());
        assertEquals(startSize.getHeight(), image.getSize().getHeight());
    }
    
}
