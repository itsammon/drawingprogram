/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Drawing.ApplicationException;
import Drawing.Drawing;
import Drawing.Image;
import Drawing.ImageExtrinsicState;
import Drawing.ImageFactory;
import java.awt.Color;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class DeselectAllCommandTest {
    private static Drawing drawing;
    private static DeselectAllCommand instance;
    private static Image image;
    
    public DeselectAllCommandTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        AddCommand add;
        try {
            drawing = new Drawing(null, Color.WHITE);
            drawing.factory = new ImageFactory();
            ImageExtrinsicState extrin = new ImageExtrinsicState();
            extrin.setType("orange");
            image = drawing.factory.getImage(extrin);
            add = new AddCommand(drawing, image);
            image.select();
            add.execute();
            instance = new DeselectAllCommand(drawing);
        } catch (IOException ex) {
            Logger.getLogger(AddCommandTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ApplicationException ex) {
            Logger.getLogger(DeselectAllCommandTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of execute method, of class DeselectAllCommand.
     */
    @Test
    public void testExecute() {
        System.out.println("execute");
        instance.execute();
        assertTrue(!image.isSelected());
    }

    /**
     * Test of undo method, of class DeselectAllCommand.
     */
    @Test
    public void testUndo() {
        System.out.println("undo");
        instance.undo();
        assertTrue(image.isSelected());
    }
    
}
