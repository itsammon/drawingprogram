/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import Drawing.Drawing;
import Drawing.Image;
import Drawing.ImageExtrinsicState;
import Drawing.ImageFactory;
import java.awt.Color;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ammon
 */
public class AddCommandTest {
    private static Drawing drawing;
    private static AddCommand instance;
    private static Image image;
    
    public AddCommandTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        try {
            drawing = new Drawing(null, Color.WHITE);
            drawing.factory = new ImageFactory();
            ImageExtrinsicState extrin = new ImageExtrinsicState();
            extrin.setType("orange");
            image = drawing.factory.getImage(extrin);
            instance = new AddCommand(drawing, image);
        } catch (IOException ex) {
            Logger.getLogger(AddCommandTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of execute method, of class AddCommand.
     */
    @Test
    public void testExecute() {
        System.out.println("execute");
        instance.execute();
        assertTrue(drawing.getImages().contains(image));
    }

    /**
     * Test of undo method, of class AddCommand.
     */
    @Test
    public void testUndo() {
        System.out.println("undo");
        instance.undo();
        assertTrue(!drawing.getImages().contains(image));
    }
    
}
